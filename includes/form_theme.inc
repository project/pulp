<?php

/*
 * Remove size attribute from all textfields
 */
function pulp_preprocess_textfield(&$variables, $hook) {
  $variables['element']['#size'] = NULL;
}

function pulp_fieldset($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper'));

  $output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';
  if (!empty($element['#title'])) {
    // Always wrap fieldset legends in a SPAN for CSS positioning.
    $output .= '<legend class="element-invisible">' . $element['#title'] . '</legend>';
    $output .= '<h3 class="pseudo-legend">' . $element['#title'] . '</h3>';
  }

  $output .= '<div class="fieldset-wrapper">';
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
  }
  $output .= $element['#children'];
  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= '</div>';
  $output .= "</fieldset>\n";
  return $output;
}